# Introduction

Another year, another FANTASITC event! I find myself sharing my experiences at KringleCon all throughout the year. While I have had the opportunity to attend several events/trainings over the years, not many compare to the practical experience and enjoyment gained at the North Pole. The event is so much fun but at the same time jammed packed full of useful tools and techniques. The best part being the collaboration between others working the challenges. Bouncing ideas off of eachother while at the same time building eachother up and sharing encouragement. There is nothing else like it!

This completes my fourth year at KringleCon and while I didn't come anywhere near close to what I had "planned" this report to look like I hope someone finds it enjoyable/informative/etc. 

The full writeup is found in my [Gitlab Repo](https://gitlab.com/arnydo/hhc21).

Sincerly,

Kyle Parrish