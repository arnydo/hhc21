# Players
## Angel Candysalt

!!! quote
    Greetings North Pole visitor! I'm Angel Candysalt!

    A euphemism? No, that's my name. Why do people ask me that?

    Anywho, I'm back at Santa’s Splunk terminal again this year.

    There's always more to learn!

    Take a look and see what you can find this year.

    With who-knows-what going on next door, it never hurts to have sharp SIEM skills!

    ...

    Yay! You did it!

    ...
    
## Bow Ninecandle

!!! quote
    Well hello! I'm Bow Ninecandle!

    Sorry I'm late to KringleCon; I got delayed by this other...  thing.

    Say, would you be interested in taking a look? We're trying to defend the North Pole systems from the Yule **Log4J**ack vulnerability.

    This terminal has everything you need to get going, and it'll walk you through the process.

    Go ahead and give it a try! No previous experience with Log4j required.

    We'll even supply a checker script in the terminal for vulnerable libraries that you could use in your own environment.

    [The talk](https://youtu.be/OuYMPU3-0p4) Prof. Petabyte is giving will be helpful too!

    Oh, and don't worry if this doesn't show up in your badge. This is just a fun extra!

    ...

    Well hello! I'm Bow Ninecandle!

    Sorry I'm late to KringleCon; I got delayed by this other...  thing.

    Say, would you be interested in taking a look? We're trying to defend the North Pole systems from the Yule **Log4J**ack vulnerability.

    This terminal has everything you need to get going, and it'll walk you through the process.

    Go ahead and give it a try! No previous experience with Log4j required.

    We'll even supply a checker script in the terminal for vulnerable libraries that you could use in your own environment.

    [The talk](https://youtu.be/OuYMPU3-0p4) Prof. Petabyte is giving will be helpful too!

    Oh, and don't worry if this doesn't show up in your badge. This is just a fun extra!

    ...
    
## Yeller

!!! quote
    HEEEEEEY YOU!!!

    ...

    GREAT JOB!

    ...
    
## Seller

!!! quote
    Your car's warranty is about to expire!

    ...

    I managed to convince the boss to give you the best price EVER!

    ...
    
## Quacker

!!! quote
    QUACK!

    ...

    BEEP BEEP!

    ...
    
## Dealer

!!! quote
    Ante up!

    ...

    You hit the jackpot!

    ...
    
## Crunchy Squishter

!!! quote
    Greetings Earthling! I'm Crunchy Squishter.

    Hey, could you help me get this device on the table working?  We've cobbled it together with primitive parts we've found on your home planet.

    We need an FPGA though - and someone who knows how to program them.

    If you haven't talked with Grody Goiterson by the Frostavator, you might get some FPGA tips there.

    ...

    Thank you! Now we're able to communicate with the rest of our people!

    ...
    
## Cyberus

!!! quote
    woof! woof!!

    WHO SAID THAT??

    I’m Cyberus, the mascot of the [SANS.edu](https://www.sans.edu/) college!

    ...

    woof! woof!!

    WHO SAID THAT??

    I’m Cyberus, the mascot of the [SANS.edu](https://www.sans.edu/) college!

    ...
    
## Jewel Loggins

!!! quote
    Well hello! I'm Jewel Loggins.

    I have to say though, I'm a bit distressed.

    The con next door? Oh sure, I’m concerned about that too, but I was talking about the issues I’m having with IPv6.

    I mean, I know it's an old protocol now, but I've just never checked it out.

    So now I'm trying to do simple things like Nmap and cURL using IPv6, and I can't quite get them working!

    Would you mind taking a look for me on this terminal?

    I think there's a Github Gist that covers tool usage with IPv6 targets.

    The tricky parts are knowing when to use [] around IPv6 addresses and where to specify the source interface.

    I’ve got a deal for you. If you show me how to solve this terminal, I’ll provide you with some nice tips about a topic I’ve been researching a lot lately – Ducky Scripts! They can be really interesting and fun!

    ...

    Well hello! I'm Jewel Loggins.

    Great work! It seems simpler now that I've seen it once. Thanks for showing me!

    Prof. Petabyte warned us about random USB devices. They might be malicious keystroke injectors!

    A troll could program a keystroke injector to deliver malicious keystrokes when it is plugged in.

    Ducky Script is a language used to specify those keystrokes.

    What commands would a troll try to run on our workstations?

    I heard that SSH keys can be used as backdoors. Maybe that's useful?

## Fitzy Shortstack

!!! quote
    Hiya, I'm Fitzy Shortstack!

    I was just trying to learn a bit more about YARA with this here Cranberry Pi terminal.

    I mean, I'm not saying I'm worried about attack threats from that other con next door, but...

    OK. I AM worried. I've been thinking a bit about how malware might bypass YARA rules.

    If you can help me solve the issue in this terminal, I’ll understand YARA so much better! Would you please check it out so I can learn?

    And, I’ll tell you what – if you help me with YARA, I’ll give you some tips for Splunk!

    I think if you make small, innocuous changes to the executable, you can get it to run in spite of the YARA rules.

    ...

    Thanks - you figured it out!

    Let me tell you what I know about Splunk.

    Did you know Splunk recently added support for new data sources including Sysmon for Linux and GitHub Audit Log data?

    Between GitHub audit log and webhook event recording, you can monitor all activity in a repository, including common `git` commands such as `git add`, `git status`, and `git commit`.

    You can also see cloned GitHub projects. There's a lot of interesting stuff out there. Did you know there are repositories of code that are Darn Vulnerable?

    Sysmon provides a lot of valuable data, but sometimes correlation across data types is still necessary.

    Sysmon network events don't reveal the process parent ID for example. Fortunately, we can pivot with a query to investigate process creation events once you get a process ID.

    Sometimes Sysmon data collection is awkward. Pipelining multiple commands generates multiple Sysmon events, for example.

    Did you know there are multiple versions of the Netcat command that can be used maliciously? `nc.openbsd`, for example.

    ...
    
## Google Booth

!!! quote
    Google is a proud sponsor of KringleCon and the Holiday Hack Challenge. We wish you a happy holiday hacking season.

    [Meet Security Engineers at Google](https://www.youtube.com/watch?v=-6ZbrfSRWKc)

    ...

    Google is a proud sponsor of KringleCon and the Holiday Hack Challenge. We wish you a happy holiday hacking season.

    [Meet Security Engineers at Google](https://www.youtube.com/watch?v=-6ZbrfSRWKc)

    ...
    
## Greasy GopherGuts

!!! quote
    Grnph. Blach! Phlegm.

    I'm Greasy Gopherguts. I need help with parsing some Nmap output.

    If you help me find some results, I'll give you some hints about Wi-Fi.

    Click on the terminal next to me and read the instructions.

    Maybe search for a cheat sheet if the hints in the terminal don't do it for ya'.

    You’ll type `quizme` in the terminal and `grep` through the Nmap bigscan.gnmap file to find answers.

    ...

    Grack. Ungh. ... Oh!

    You really did it?

    Well, OK then. Here's what I know about the wifi here.

    Scanning for Wi-Fi networks with iwlist will be location-dependent. You may need to move around the North Pole and keep scanning to identify a Wi-Fi network.

    Wireless in Linux is supported by many tools, but `iwlist` and `iwconfig` are commonly used at the command line.

    The `curl` utility can make HTTP requests at the command line!

    By default, `curl` makes an HTTP GET request. You can add `--request POST` as a command line argument to make an HTTP POST request.

    When sending HTTP POST, add `--data-binary` followed by the data you want to send as the POST body.

    ...
    
## Grimy McTrollkins

!!! quote
    Yo, I'm Grimy McTrollkins.

    I'm a troll and I work for the big guy over there: Jack Frost.

    I’d rather not be bothered talking with you, but I’m kind of in a bind and need your help.

    Jack Frost is so obsessed with icy cold that he accidentally froze shut the door to Frost Tower!

    I wonder if you can help me get back in.

    I think we can melt the door open if we can just get access to the thermostat inside the building.

    That thermostat uses Wi-Fi.  And I’ll bet you picked up a Wi-Fi adapter for your badge when you got to the North Pole.

    Click on your badge and go to the **Items** tab.  There, you should see your Wi-Fi Dongle and a button to “Open Wi-Fi CLI.”  That’ll give you command-line interface access to your badge’s wireless capabilities.

    ...

    Great - now I can get back in!

    ...
    
## Grody Goiterson

!!! quote
    Hrmph. Snrack! Pthbthbthb.

    Gnerphk. Well, on to business.

    I'm Grody Goiterson. ... It's a family name.

    So hey, this is the Frostavator. It runs on some logic chips... that fell out.

    I put them back in, but I must have mixed them up, because it isn't working now.

    If you don't know much about logic gates, it's something you should look up.

    If you help me run the elevator, maybe I can help you with something else.

    I'm pretty good with FPGAs, if that's worth something to ya'.

    ...

    Oooo... That's it!

    A deal's a deal. Let's talk FPGA.

    First, did you know there are people who do this stuff [for fun](https://www.fpga4fun.com/MusicBox.html)??

    I mean, I'm more into picking on other trolls for fun, but whatever.

    Also, that Prof. Petabyte guy is giving [a talk](https://www.youtube.com/watch?v=GFdG1PJ4QjA) about FPGAs. Weirdo.

    So hey, good luck or whatever.

    ...
    
## Jason

!!! quote
    Hey! It's me, Jason!

    Job Hunting? Feeling stuck?

    759, 760, 761!

    ...

    Hey! It's me, Jason!

    Job Hunting? Feeling stuck?

    759, 760, 761!

    ...
    
## Hubris Selfington

!!! quote
    Snarf. Hrung. Phlthth.

    I'm Hubris Selfington.

    The big boss told me he’s worried about vulnerabilities in his slot machines, especially this one.

    Statistically speaking, it seems to be paying out way too much.

    He asked me to see if there are any security flaws in it.

    The boss has HUGE plans and we’ve gotta make sure we are running a tight ship here at Frost Tower.

    Can you help me find the issue?

    I mean, I could TOTALLY do this on my own, but I want to give you a chance first.

    ...

    Yeah, that's exactly how I would have solved it, but thanks.

    ...
    
## Icky McGoop

!!! quote
    Hey, I'm Icky McGoop.

    Late? What's it to you?  I got here when I got here.

    So anyways, I thought you might be interested in this Yule **Log4J**ack.  It's all the rage lately.

    Yule **Log4J**ack is in a ton of software - helps our big guy keep track of things.

    It's kind of like salt. It's in WAY more things than you normally think about.

    In fact, a vulnerable Solr instance is running in an internal North Pole system, accessible in this terminal.

    Anyways, why don't you see if you can get to the `yule.log` file in this system?

    ...

    Hey, I'm Icky McGoop.

    Late? What's it to you?  I got here when I got here.

    So anyways, I thought you might be interested in this Yule **Log4J**ack.  It's all the rage lately.

    Yule **Log4J**ack is in a ton of software - helps our big guy keep track of things.

    It's kind of like salt. It's in WAY more things than you normally think about.

    In fact, a vulnerable Solr instance is running in an internal North Pole system, accessible in this terminal.

    Anyways, why don't you see if you can get to the `yule.log` file in this system?

    ...
    
## Ingreta Tude

!!! quote
    Hey there!  I’m Ingreta Tude.  I really don’t like the direction Jack Frost is leading us.

    He seems obsessed with beating Santa and taking over the holiday season.  It just doesn’t seem right.

    Why can’t we work together with Santa and the elves instead of trying to beat them?

    But, I do have an Objective for you.  We’re getting ready to launch a new website for Frost Tower, and the big guy has charged me with making sure it’s secure.

    My sister, Ruby Cyster, created this site, and I don’t trust the results.

    Can you please take a look at it to find flaws?

    Here is the [source code](https://download.holidayhackchallenge.com/2021/frosttower-web.zip) if you need it.

    ...

    Oh wow - I thought we left SQL injection in the last decade.

    Thanks for your help finding this!

    ...
    
## Jack Frost

!!! quote
    Welcome to the North Pole - the Frostiest Place on Earth™!

    Last year, Santa somehow foiled my plot.

    So this year, I've decided to beat Santa at his own game – I’m gonna take over the Holiday Season from the old man and dominate it myself.

    I've built Frost Tower, the epicenter of Frostiness at the North Pole.  Believe me, it's the BIGGEST North Pole tower the world has EVER seen! So much better than that lame castle next door.

    And, quite frankly, our FrostFest conference is going to be the GREATEST con in the history of cons.

    As for FrostFest, we honor all badges for entry, including those from the lame conference next door.

    Oh, and make sure you visit the gift shop and buy some SWAG on your way out.

    Everybody says it's the best SWAG you'll ever find!  People love our swag!

    ...

    Welcome to the North Pole - the Frostiest Place on Earth™!

    Last year, Santa somehow foiled my plot.

    So this year, I've decided to beat Santa at his own game – I’m gonna take over the Holiday Season from the old man and dominate it myself.

    I've built Frost Tower, the epicenter of Frostiness at the North Pole.  Believe me, it's the BIGGEST North Pole tower the world has EVER seen! So much better than that lame castle next door.

    And, quite frankly, our FrostFest conference is going to be the GREATEST con in the history of cons.

    As for FrostFest, we honor all badges for entry, including those from the lame conference next door.

    Oh, and make sure you visit the gift shop and buy some SWAG on your way out.

    Everybody says it's the best SWAG you'll ever find!  People love our swag!

    ...
    
## Jack Frost

!!! quote
    Find the true meaning of the holidays.

    Spend money, money, money on spectacular and adorable gifts for the whole family!

    Click on any of the items in the gift shop.

    For those that are available, you'll be whisked to our fast and efficient online shop, the most beautiful online shop ever.

    Believe me.

    All of the other items that are not clickable are stranded on a container ship due to supply chain constraints.

    ...

    Find the true meaning of the holidays.

    Spend money, money, money on spectacular and adorable gifts for the whole family!

    Click on any of the items in the gift shop.

    For those that are available, you'll be whisked to our fast and efficient online shop, the most beautiful online shop ever.

    Believe me.

    All of the other items that are not clickable are stranded on a container ship due to supply chain constraints.

    ...
    
## Jack Frost

!!! quote
    Welcome to Frost Tower and Casino, the epicenter of the Frostiest Place on Earth™!

    We’ll be running the Holiday Season from this point on, doing things far better than those amateurs at Santa’s castle.

    Sadly, they just don’t understand the true meaning of the holidays.

    Feel free to explore, place some bets on certain slot machines, and visit the gift store on your way out to shop to your heart's content.  Money, money, money!

    That's the true meaning of the holiday season.

    And don't forget: Tell all your friends to come to FrostFest and stay away from that lame con next door!

    ...

    Welcome to Frost Tower and Casino, the epicenter of the Frostiest Place on Earth™!

    We’ll be running the Holiday Season from this point on, doing things far better than those amateurs at Santa’s castle.

    Sadly, they just don’t understand the true meaning of the holidays.

    Feel free to explore, place some bets on certain slot machines, and visit the gift store on your way out to shop to your heart's content.  Money, money, money!

    That's the true meaning of the holiday season.

    And don't forget: Tell all your friends to come to FrostFest and stay away from that lame con next door!

    ...
    
## Jingle Ringford

!!! quote
    Welcome to the North Pole, KringleCon, and the 2021 SANS Holiday Hack Challenge!  I’m Jingle Ringford, one of Santa’s elves.

    Santa asked me to come here and give you a short orientation to this festive event.

    Before you move forward through the gate, I’ll ask you to accomplish a few simple tasks.

    First things first, here's your badge! It's that wrapped present in the middle of your avatar.

    Great - now you're official! 

    Click on the badge on your avatar 🎁.  That’s where you will see your Objectives, Hints, and gathered Items for the Holiday Hack Challenge.

    We’ve also got handy links to the KringleCon talks and more there for you!

    Next, click on that USB wifi adapter - just in case you need it later.

    ...

    Great!  Your orientation is now complete!  You can enter through the gate now.  Have FUN!!!

    ...
    
## Noel Boetie

!!! quote
    Hello there!  Noel Boetie here.  We’re all so glad to have you attend KringleCon IV and work on the Holiday Hack Challenge!

    I'm just hanging out here by the Logic Munchers game.

    You know… logic: that thing that seems to be in short supply at the tower on the other side of the North Pole?

    Oh, I'm sorry. That wasn't terribly kind, but those frosty souls do confuse me...

    Anyway, I’m working my way through this Logic Munchers game.

    A lot of it comes down to understanding boolean logic, like `True And False` is `False`, but `True And True` is `True`.

    It _can_ get a tad complex in the later levels.

    I need some help, though.  If you can show me how to complete a stage in Potpourri at the Intermediate (Stage 3) or higher, I’ll give you some hints for how to find vulnerabilities.

    Specifically, I’ll give you some tips in finding flaws in some of the web applications I’ve heard about here at the North Pole, especially those associated with slot machines!

    ...

    Wow - amazing score! Great work!

    So hey, those slot machines. It seems that in his haste, Jack bought some terrible hardware.

    It seems they're susceptible to [parameter tampering](https://owasp.org/www-community/attacks/Web_Parameter_Tampering).

    You can modify web request parameters with an intercepting proxy or tools built into Firefox.

    ...
    
## Noxious O. D'or

!!! quote
    Hey, this is the executive restroom. Wasn't that door closed?

    I’m Noxious O’Dor.  And I’ve gotta say, I think that Jack Frost is just messed up.

    I mean, I'm no expert, but his effort to "win" against Santa by going bigger and bolder seems bad.

    You know, I’m having some trouble with this IMDS exploration.  I’m hoping you can give me some help in solving it.

    If you do, I’ll be happy to trade you for some hints on SSRF!  I’ve been studying up on that and have some good ideas on how to attack it!

    ...

    Phew! That is something extra! Oh, and you solved the challenge too? Great!

    Cloud assets are interesting targets for attackers. Did you know they automatically get IMDS access?

    I'm very concerned about the combination of SSRF and IMDS access.

    Did you know it's possible to harvest cloud keys through SSRF and IMDS attacks?

    Dr. Petabyte told us, "anytime you see URL as an input, test for SSRF."

    With an SSRF attack, we can make the server request a URL. This can reveal valuable data!

    The [AWS documentation for IMDS](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html) is interesting reading.

    ...
    
## Numby Chilblain

!!! quote
    Klatu Barada Nikto!

    I'm Numby Chilblain.

    ...

    Oh, did you notice that spaceship there?

    The device you helped fix summoned it AT LAST!

    Finally, this madness Jack Frost stirred up will soon come to an end.

    ...
    
## Pat Tronizer

!!! quote
    Hrmph. Oh hey, I'm Pat Tronizer.

    I'm SO glad to have all these first-rate talks here.

    We issued a Call for Talks, but only one person responded… We put him in track 1.

    But Jack came up with an ingenious way to borrow additional talks for FrostFest! You can hardly tell where we got these great speakers!

    Anyway, I cannot believe an actual human [connected to the Tower network](https://downloads.jackfrosttower.com/2021/jackfrosttower-network.zip).  It’s supposed to be the domain of us trolls and of course Jack Frost himself.

    Mr. Frost has a strict policy: all devices must be [RFC3514](https://datatracker.ietf.org/doc/html/rfc3514) compliant.  It fits in with our nefarious plans.

    Some human had the nerve to use our complaint website to submit a complaint!

    That website is for trolls to complain about guests, NOT the other way around.

    Humans have some nerve.

    ...

    Hrmph. Oh hey, I'm Pat Tronizer.

    I'm SO glad to have all these first-rate talks here.

    We issued a Call for Talks, but only one person responded… We put him in track 1.

    But Jack came up with an ingenious way to borrow additional talks for FrostFest! You can hardly tell where we got these great speakers!

    Anyway, I cannot believe an actual human [connected to the Tower network](https://downloads.jackfrosttower.com/2021/jackfrosttower-network.zip).  It’s supposed to be the domain of us trolls and of course Jack Frost himself.

    Mr. Frost has a strict policy: all devices must be [RFC3514](https://datatracker.ietf.org/doc/html/rfc3514) compliant.  It fits in with our nefarious plans.

    Some human had the nerve to use our complaint website to submit a complaint!

    That website is for trolls to complain about guests, NOT the other way around.

    Humans have some nerve.

    ...
    
## Piney Sappington

!!! quote
    Hi ho, Piney Sappington at your service!

    Well, honestly, I could use a touch of your services.

    You see, I've been looking at these documents, and I know someone has tampered with one file.

    Do you think you could log into this Cranberry Pi and take a look?

    It has `exiftool` installed on it, if that helps you at all.

    I just... Well, I have a feeling that someone at that other conference might have fiddled with things.

    And, if you help me figure this tampering issue out, I’ll give you some hints about OSINT, especially associated with geographic locations!

    ...

    Wow, you figured that out in no time! Thanks!

    I _knew_ they were up to no good.

    So hey, have you tried the Caramel Santaigo game in this courtyard?

    Carmen? No, I haven't heard of her.

    So anyway, some of the hints use obscure coordinate systems like [MGRS](https://en.wikipedia.org/wiki/Military_Grid_Reference_System) and even [what3words](https://what3words.com/).

    In some cases, you might get an image with location info in the metadata. Good thing you know how to see that stuff now!

    (And they say, for those who _don't_ like gameplay, there might be a way to bypass by looking at some flavor of cookie...)

    And Clay Moody is giving a talk on OSINT techniques right now!

    Oh, and don't forget to learn about your target elf and filter in the Interrink system!

    ...
    
## Ribb Bonbowford

!!! quote
    Hello, I'm Ribb Bonbowford. Nice to meet you!

    Are you new to programming? It's a handy skill for anyone in cyber security.

    This here machine lets you control an Elf using Python 3.  It’s pretty fun, but I’m having trouble getting beyond Level 8.

    Tell you what… if you help me get past Level 8, I’ll share some of my SQLi tips with you.  You may find them handy sometime around the North Pole this season.

    Most of the information you'll need is provided during the game, but I'll give you a few more pointers, if you want them.

    Not sure what a lever requires? Click it in the `Current Level Objectives` panel.

    You can move the elf with commands like `elf.moveLeft(5)`, `elf.moveTo({"x":2,"y":2})`, or `elf.moveTo(lever0.position)`.

    Looping through long movements? Don't be afraid to `moveUp(99)` or whatever. You elf will stop at any obstacle.

    You can call functions like `myFunction()`. If you ever need to pass a function to a munchkin, you can use `myFunction` without the `()`.

    ...

    Gosh, with skills like that, I'll bet you could help figure out what's really going on next door...

    And, as I promised, let me tell you what I know about SQL injection.

    I hear that having source code for vulnerability discovery dramatically changes the vulnerability discovery process.

    I imagine it changes how you approach an assessment too.

    When you have the source code, API documentation becomes [tremendously](https://www.npmjs.com/package/express-session) [valuable](https://github.com/mysqljs/mysql).

    Who knows? Maybe you'll even find more than one vulnerability in the code.

    ...
    
## Rose Mold

!!! quote
    I'm Rose Mold. What planet are _you_ from?

    Hey, way to go climbing the stairs. You do know you can teleport, right?

    Or just use the Frostavator.

    n00bs...

    ...

    I'm Rose Mold. What planet are _you_ from?

    Hey, way to go climbing the stairs. You do know you can teleport, right?

    Or just use the Frostavator.

    n00bs...

    ...
    
## RSAC Booth

!!! quote
    Happy Holidays! Join us February 7-10 for the new RSAC 2022.

    [Click to learn more](https://www.rsaconference.com/usa) about our program.

    ...

    Happy Holidays! Join us February 7-10 for the new RSAC 2022.

    [Click to learn more](https://www.rsaconference.com/usa) about our program.

    ...
    
## Ruby Cyster

!!! quote
    Hey, I'm Ruby Cyster. Don't listen to anything my sister, Ingreta, says about me.

    So I'm looking at this system, and it has me a _little_ bit worried.

    If I didn't know better, I'd say someone here is learning how to hack North Pole systems.

    Who's got that kind of nerve!

    Anyway, I hear some elf on the other roof knows a bit about this type of thing.

    ...

    Oh man - what **is** this all about? Great work though.

    So first things first, you should definitely take a look at the firmware.

    With that in-hand, you can pick it apart and see what's there.

    Did you know that if you append multiple files of that type, the last one is processed?

    Have you heard of [Hash Extension Attacks](https://blog.skullsecurity.org/2012/everything-you-need-to-know-about-hash-length-extension-attacks)?

    If something isn't working, be sure to check the output! The error messages are very verbose.

    Everything else accomplished, you just might be able to get shell access to that dusty old thing!

    ...
    
## SANS.edu Booth

!!! quote
    Happy holidays from the best college in cybersecurity!

    Interested in attending?  Here's a code to waive your application fee to [SANS.edu](https://www.sans.edu/), the best college in cybersecurity.

    Use the code `SantaGoesToCollege` when you apply.

    ...

    Happy holidays from the best college in cybersecurity!

    Interested in attending?  Here's a code to waive your application fee to [SANS.edu](https://www.sans.edu/), the best college in cybersecurity.

    Use the code `SantaGoesToCollege` when you apply.

    ...
    
## Santa

!!! quote
    Ho ho ho! I'm Santa Claus!

    Welcome to the North Pole and KringleCon IV: Calling Birds!

    I’d like to introduce you to the four birds here, each of whom is calling.

    We're so glad to have you here to celebrate the holidays - and practice some important skills.

    What's that? You've heard of another conference up at the North Pole?

    Well, I'm afraid you'll have to ask Jack Frost about that.

    To be honest, I'm not quite sure _what_ his intentions are, but I am keeping an eye out...

    Anyway, enjoy your time with the SANS Holiday Hack Challenge and KringleCon!

    ...

    Ho ho ho! I'm Santa Claus!

    Welcome to the North Pole and KringleCon IV: Calling Birds!

    I’d like to introduce you to the four birds here, each of whom is calling.

    We're so glad to have you here to celebrate the holidays - and practice some important skills.

    What's that? You've heard of another conference up at the North Pole?

    Well, I'm afraid you'll have to ask Jack Frost about that.

    To be honest, I'm not quite sure _what_ his intentions are, but I am keeping an eye out...

    Anyway, enjoy your time with the SANS Holiday Hack Challenge and KringleCon!

    ...
    
## Santa

!!! quote
    Welcome to my castle and KringleCon!

    Please make yourself at home, chat with other guests, and visit the talk rooms on the second floor.

    Also, you may find elves who need your help with various tech problems around the castle.

    If you help them, they’ll give you some tips on how to solve the Holiday Hack Challenge Objectives in your badge.

    Above all, have fun!

    ...

    Welcome to my castle and KringleCon!

    Please make yourself at home, chat with other guests, and visit the talk rooms on the second floor.

    Also, you may find elves who need your help with various tech problems around the castle.

    If you help them, they’ll give you some tips on how to solve the Holiday Hack Challenge Objectives in your badge.

    Above all, have fun!

    ...
    
## Sparkle Redberry

!!! quote
    Hey there! I’m Sparkle Redberry.

    This year, the Santavator is in top working shape!  We ironed out all of the issues from last year with it.

    As for that tower next door, I hear they have an elevator of some sort too.

    I just don't know if it would take me anywhere I'd really want to go.

    ...

    Hey there! I’m Sparkle Redberry.

    This year, the Santavator is in top working shape!  We ironed out all of the issues from last year with it.

    As for that tower next door, I hear they have an elevator of some sort too.

    I just don't know if it would take me anywhere I'd really want to go.

    ...
    
## Splunk Booth

!!! quote
    Splunk is proud to be a contributor to KringleCon and the Holiday Hack Challenge. Happy Holidays from the Splunk security team!

    ...

    Splunk is proud to be a contributor to KringleCon and the Holiday Hack Challenge. Happy Holidays from the Splunk security team!

    ...
    
## Tangle Coalbox

!!! quote
    Hey there, Gumshoe. Tangle Coalbox here again.

    I've got a real doozy of a case for you this year.

    Turns out some elves have gone on some misdirected journeys around the globe. It seems that someone is messing with their travel plans.

    We could sure use your open source intelligence (OSINT) skills to find them.

    Why dontcha' log into this vintage Cranberry Pi terminal and see if you have what it takes to track them around the globe.

    If you're having any trouble with it, you might ask Piney Sappington right over there for tips.

    ...

    You never cease to amaze, Kid. Thanks for your help.

    ...
    
## Tinsel Upatree

!!! quote
    Hiya hiya, I'm Tinsel Upatree!

    Say, do you know what's going on next door?

    I'm a bit worried about the whole FrostFest event.

    It feels a bit... ill-conceived, somehow. Nasty even.

    Well, regardless – and more to the point, what do you know about tracing processes in Linux?

    We rebuilt this here Cranberry Pi that runs the cotton candy machine, but we seem to be missing a file.

    Do you think you can use `strace` or `ltrace` to help us rebuild the missing config?

    We'd like to help some of our favorite children enjoy the sweet spun goodness again!

    And, if you help me with this, I’ll give you some hints about using Wireshark filters to look for unusual options that might help you achieve Objectives here at the North Pole.

    ...

    Great! Thanks so much for your help!

    I'm sure I can put those skills I just learned from you to good use.

    Are you familiar with [RFC3514](https://datatracker.ietf.org/doc/html/rfc3514)?

    [Wireshark](https://www.wireshark.org/) uses a different name for the Evil Bit: `ip.flags.rb`.

    HTTP responses are often gzip compressed. Fortunately, Wireshark decompresses them for us automatically.

    You can search for strings in Wireshark fields using [display filters](https://wiki.wireshark.org/DisplayFilters) with the `contains` keyword.

    ...
    
