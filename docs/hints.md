# Hints

## AWS IMDS Documentation

From: Noxious O. D'or

Objective: 10) Now Hiring!

!!! hint
    The [AWS documentation for IMDS](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/instancedata-data-retrieval.html) is interesting reading.



## Wireshark Display Filters

From: Tinsel Upatree

Objective: 11) Customer Complaint Analysis

!!! hint
    Different from BPF capture filters, Wireshark's [display filters](https://wiki.wireshark.org/DisplayFilters) can find text with the `contains` keyword - and evil bits with `ip.flags.rb`.



## Evil Bit RFC

From: Tinsel Upatree

Objective: 11) Customer Complaint Analysis

!!! hint
    [RFC3514](https://datatracker.ietf.org/doc/html/rfc3514) defines the usage of the "Evil Bit" in IPv4 headers.



## SQL Injection with Source

From: Ribb Bonbowford

Objective: 12) Frost Tower Website Checkup

!!! hint
    When you have the source code, API documentation becomes [tremendously](https://www.npmjs.com/package/express-session) [valuable](https://github.com/mysqljs/mysql).



## FPGA Talk

From: Grody Goiterson

Objective: 13) FPGA Programming

!!! hint
    Prof. Qwerty Petabyte is giving [a lesson](https://www.youtube.com/watch?v=GFdG1PJ4QjA) about Field Programmable Gate Arrays (FPGAs).



## FPGA for Fun

From: Grody Goiterson

Objective: 13) FPGA Programming

!!! hint
    There are [FPGA enthusiast sites](https://www.fpga4fun.com/MusicBox.html).



## Coordinate Systems

From: Piney Sappington

Objective: 2) Where in the World is Caramel Santaigo?

!!! hint
    Don't forget coordinate systems other than lat/long like [MGRS](https://en.wikipedia.org/wiki/Military_Grid_Reference_System) and [what3words](https://what3words.com/).



## Flask Cookies

From: Piney Sappington

Objective: 2) Where in the World is Caramel Santaigo?

!!! hint
    While Flask cookies can't generally be forged without the secret, they can often be [decoded and read](https://gist.github.com/chriselgee/b9f1861dd9b99a8c1ed30066b25ff80b).



## OSINT Talk

From: Piney Sappington

Objective: 2) Where in the World is Caramel Santaigo?

!!! hint
    Clay Moody is giving [a talk](https://www.youtube.com/watch?v=tAot_mcBT9c) about OSINT techniques right now!



## Linux Wi-Fi Commands

From: Greasy GopherGuts

Objective: 3) Thaw Frost Tower's Entrance

!!! hint
    The [iwlist](https://linux.die.net/man/8/iwlist) and [iwconfig](https://linux.die.net/man/8/iwconfig) utilities are key for managing Wi-Fi from the Linux command line.



## Adding Data to cURL requests

From: Greasy GopherGuts

Objective: 3) Thaw Frost Tower's Entrance

!!! hint
    When sending a [POST request with data](https://www.educative.io/edpresso/how-to-perform-a-post-request-using-curl), add `--data-binary` to your `curl` command followed by the data you want to send.



## Web Browsing with cURL

From: Greasy GopherGuts

Objective: 3) Thaw Frost Tower's Entrance

!!! hint
    [cURL](https://linux.die.net/man/1/curl) makes HTTP requests from a terminal - in Mac, Linux, and modern Windows!



## Parameter Tampering

From: Noel Boetie

Objective: 4) Slot Machine Investigation

!!! hint
    It seems they're susceptible to [parameter tampering](https://owasp.org/www-community/attacks/Web_Parameter_Tampering).



## Intercepting Proxies

From: Noel Boetie

Objective: 4) Slot Machine Investigation

!!! hint
    Web application testers can use tools like [Burp Suite](https://portswigger.net/burp/communitydownload) or even right in the browser with Firefox's [Edit and Resend](https://itectec.com/superuser/how-to-edit-parameters-sent-through-a-form-on-the-firebug-console/) feature.



## Duck Encoder

From: Jewel Loggins

Objective: 5) Strange USB Device

!!! hint
    Attackers can encode Ducky Script using a [duck encoder](https://docs.hak5.org/hc/en-us/articles/360010471234-Writing-your-first-USB-Rubber-Ducky-Payload) for delivery as `inject.bin`.



## Ducky Script

From: Jewel Loggins

Objective: 5) Strange USB Device

!!! hint
    [Ducky Script](https://docs.hak5.org/hc/en-us/articles/360010555153-Ducky-Script-the-USB-Rubber-Ducky-language) is the language for the USB Rubber Ducky



## Ducky RE with Mallard

From: Jewel Loggins

Objective: 5) Strange USB Device

!!! hint
    It's also possible the reverse engineer encoded Ducky Script using [Mallard](https://github.com/dagonis/Mallard).



## Mitre ATT&CK™ and Ducky

From: Jewel Loggins

Objective: 5) Strange USB Device

!!! hint
    The [MITRE ATT&CK™ tactic T1098.004](https://attack.mitre.org/techniques/T1098/004/) describes SSH persistence techniques through authorized keys files.



## Register Stomping

From: Chimney Scissorsticks

Objective: 6) Shellcode Primer

!!! hint
    Lastly, be careful not to overwrite any register values you need to reference later on in your shellcode.



## Debugging Shellcode

From: Chimney Scissorsticks

Objective: 6) Shellcode Primer

!!! hint
    Also, troubleshooting shellcode can be difficult. Use the debugger step-by-step feature to watch values.



## Shellcode Primer Primer

From: Chimney Scissorsticks

Objective: 6) Shellcode Primer

!!! hint
    If you run into any shellcode primers at the North Pole, be sure to read the directions and the comments in the shellcode source!



## Dropping Files

From: Ruby Cyster

Objective: 7) Printer Exploitation

!!! hint
    Files placed in `/app/lib/public/incoming` will be accessible under [https://printer.kringlecastle.com/incoming/](https://printer.kringlecastle.com/incoming/).



## Hash Extension Attacks

From: Ruby Cyster

Objective: 7) Printer Exploitation

!!! hint
    [Hash Extension Attacks](https://blog.skullsecurity.org/2012/everything-you-need-to-know-about-hash-length-extension-attacks) can be super handy when there's some type of validation to be circumvented.



## Printer Firmware

From: Ruby Cyster

Objective: 7) Printer Exploitation

!!! hint
    When analyzing a device, it's always a good idea to pick apart the firmware. Sometimes these things come down Base64-encoded.



## Finding Domain Controllers

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    There will be some `10.X.X.X` networks in your routing tables that may be interesting. Also, consider adding `-PS22,445` to your `nmap` scans to "fix" default probing for unprivileged scans.



## CeWL for Wordlist Creation

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    [CeWL](https://github.com/digininja/CeWL) can generate some great wordlists from website, but it will ignore digits in terms by default.



## Kerberoast and AD Abuse Talk

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    Check out [Chris Davis' talk](https://www.youtube.com/watch?v=iMh8FTzepU4) [and scripts](https://github.com/chrisjd20/hhc21_powershell_snippets) on Kerberoasting and Active Directory permissions abuse.



## Hashcat Mangling Rules

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    [OneRuleToRuleThemAll.rule](https://github.com/NotSoSecure/password_cracking_rules) is great for mangling when a password dictionary isn't enough.



## Kerberoasting and Hashcat Syntax

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    Learn about [Kerberoasting](https://gist.github.com/TarlogicSecurity/2f221924fef8c14a1d8e29f3cb5c5c4a) to leverage domain credentials to get usernames and crackable hashes for service accounts.



## Stored Credentials

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    Administrators often store credentials in scripts. These can be coopted by an attacker for other purposes!



## Active Directory Interrogation

From: Eve Snowshoes

Objective: 8) Kerberoasting on an Open Fire

!!! hint
    Investigating Active Directory errors is harder without [Bloodhound](https://github.com/BloodHoundAD/BloodHound), but there are [native](https://social.technet.microsoft.com/Forums/en-US/df3bfd33-c070-4a9c-be98-c4da6e591a0a/forum-faq-using-powershell-to-assign-permissions-on-active-directory-objects?forum=winserverpowershell) [methods](https://www.specterops.io/assets/resources/an_ace_up_the_sleeve.pdf).



## Sysmon Monitoring in Splunk

From: Fitzy Shortstack

Objective: 9) Splunk!

!!! hint
    Sysmon network events don't reveal the process parent ID for example. Fortunately, we can pivot with a query to investigate process creation events once you get a process ID.



## GitHub Monitoring in Splunk

From: Fitzy Shortstack

Objective: 9) Splunk!

!!! hint
    Between GitHub audit log and webhook event recording, you can monitor all activity in a repository, including common `git` commands such as `git add`, `git status`, and `git commit`.



## Malicious NetCat??

From: Fitzy Shortstack

Objective: 9) Splunk!

!!! hint
    Did you know there are multiple versions of the Netcat command that can be used maliciously? `nc.openbsd`, for example.



## Log4j Talk

From: Bow Ninecandle

Terminal: Bonus! Blue Log4Jack

!!! hint
    Prof. Qwerty Petabyte is giving [a lesson](https://youtu.be/OuYMPU3-0p4) about Apache Log4j.



## Log4J at Apache

From: Bow Ninecandle

Terminal: Bonus! Blue Log4Jack

!!! hint
    Software by the [Apache Foundation](https://logging.apache.org/log4j/2.x/manual/lookups.html) runs on devices all over the internet



## Log4j Search Script

From: Bow Ninecandle

Terminal: Bonus! Blue Log4Jack

!!! hint
    Josh Wright's [simple checker script](https://gist.github.com/joswr1ght/a6badf9b0b148efadfccbf967fcc2b41) uses the power of regex to find vulnerable Log4j libraries!



## Log4j Discussion with Bishop Fox

From: Icky McGoop

Terminal: Bonus! Red Log4Jack

!!! hint
    Join Bishop Fox for [a discussion](https://bishopfox.com/blog/log4j-zero-day-cve-2021-44228) of the issues involved.



## Log4j Red Help Document

From: Icky McGoop

Terminal: Bonus! Red Log4Jack

!!! hint
    Josh Wright's [help document](https://gist.github.com/joswr1ght/fb361f1f1e58307048aae5c0f38701e4) for the Red challenge.



## Function Calls

From: Ribb Bonbowford

Terminal: Elf Code Python

!!! hint
    You can call functions like `myFunction()`. If you ever need to pass a function to a munchkin, you can use `myFunction` without the `()`.



## Bumping into Walls

From: Ribb Bonbowford

Terminal: Elf Code Python

!!! hint
    Looping through long movements? Don't be afraid to `moveUp(99)` or whatever. You elf will stop at any obstacle.



## Moving the Elf

From: Ribb Bonbowford

Terminal: Elf Code Python

!!! hint
    You can move the elf with commands like `elf.moveLeft(5)`, `elf.moveTo({"x":2,"y":2})`, or `elf.moveTo(lever0.position)`.



## Lever Requirements

From: Ribb Bonbowford

Terminal: Elf Code Python

!!! hint
    Not sure what a lever requires? Click it in the `Current Level Objectives` panel.



## Logic Gate Iconography

From: Grody Goiterson

Terminal: Frostavator

!!! hint
    [This](https://www.geeksforgeeks.org/introduction-of-logic-gates/)



## Grep Cheat Sheet

From: Greasy GopherGuts

Terminal: Grepping for Gold

!!! hint
    Check [this](https://ryanstutorials.net/linuxtutorial/cheatsheetgrep.php) out if you need a `grep` refresher.



## IPv6 Reference

From: Jewel Loggins

Terminal: IPv6 Sandbox

!!! hint
    Check out [this Github Gist](https://gist.github.com/chriselgee/c1c69756e527f649d0a95b6f20337c2f) with common tools used in an IPv6 context.



## AND, OR, NOT, XOR

From: Noel Boetie

Terminal: Logic Munchers

!!! hint
    [This](http://www.natna.info/English/Teaching/CSI30-materials/Chapter1-cheat-sheet.pdf) might be a handy reference too.



## Boolean Logic

From: Noel Boetie

Terminal: Logic Munchers

!!! hint
    There are lots of special symbols for logic and set notation. [This one](http://notes.imt-decal.org/sets/cheat-sheet.html) covers AND, NOT, and OR at the bottom.


