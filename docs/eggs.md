# Eggs

## Frost Portrait

Notice anything odd about the paintings? They were all kinda bland...but had similar colors, etc.

![](./images/2022-01-07-23-57-23.png)

## Printer Secret Endpoint

```
https://printer.kringlecastle.com/secretendpointforuptime

Alabasters secret elf pass: Iw1shi7w@$X-masEv3ryDay\!
```

## Shaningans

Secret floor found in the JS files for the elevator.

```
targetFloor = 6


  $.ajax({
    type: 'POST',
    url: POST_URL,
    dataType: 'json',
    contentType: 'application/json',
    data: JSON.stringify({
      targetFloor,
      id: getParams.id,
    }),
    success: (res, status) => {
      if (res.hash) {
        __POST_RESULTS__({
          resourceId: getParams.id || '1111',
          hash: res.hash,
          action: `goToFloor-${targetFloor}`,
        });
        closeChallenge();
      }
    }
  });
```

![](./images/2022-01-07-23-59-34.png)

## Dennis Nedry

When logging into Kringlecon from another location this is seen in the Websocket
![](./images/nedry1.png)
![](./images/nedry2.png)

## Goodbye

![](./images/goodbye.png)

## Elf University

![](./images/elfuniversity.png)
![](./images/elfuniversitytranslate.png)
