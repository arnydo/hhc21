# Terminals

## Yara

[https://docker2021.kringlecon.com/?challenge=yara](https://docker2021.kringlecon.com/?challenge=yara)

```console
snowball2@8966446e625a:~$ ./the_critical_elf_app
yara_rule_135 ./the_critical_elf_app
snowball2@8966446e625a:~$ grep yara_rule_135 yara_rules/rules.yar -a 30
```

```yara
rule yara_rule_135 {
   meta:
      description = "binaries - file Sugar_in_the_machinery"
      author = "Sparkle Redberry"
      reference = "North Pole Malware Research Lab"
      date = "1955-04-21"
      hash = "19ecaadb2159b566c39c999b0f860b4d8fc2824eb648e275f57a6dbceaf9b488"
   strings:
      $s = "candycane"
   condition:
      $s
}
```

```
snowball2@8966446e625a:~$ sed -i 's/candycane/enacydnac/g' the_critical_elf_app
snowball2@8966446e625a:~$ ./the_critical_elf_app
yara_rule_1056 ./the_critical_elf_app
```

```
snowball2@add0980fc637:~$ grep -a30 yara_rule_1056  yara_rules/rules.yar

rule yara_rule_1056 {
   meta:
        description = "binaries - file frosty.exe"
        author = "Sparkle Redberry"
        reference = "North Pole Malware Research Lab"
        date = "1955-04-21"
        hash = "b9b95f671e3d54318b3fd4db1ba3b813325fcef462070da163193d7acb5fcd03"
    strings:
        $s1 = {6c 6962 632e 736f 2e36}
        $hs2 = {726f 6772 616d 2121}
    condition:
        all of them
}
```

```
snowball2@b738807e48f7:~$ xxd < the_critical_elf_app | sed 's/726f 6772 616d 2121/726f 6772 616d 2120/' | xxd -r > the_critical_elf_app2
```

```
snowball2@2ab441da8569:~$ ./the_critical_elf_app2
yara_rule_1732 ./the_critical_elf_app2
```

```
rule yara_rule_1732 {
   meta:
      description = "binaries - alwayz_winter.exe"
      author = "Santa"
      reference = "North Pole Malware Research Lab"
      date = "1955-04-22"
      hash = "c1e31a539898aab18f483d9e7b3c698ea45799e78bddc919a7dbebb1b40193a8"
   strings:
      $s1 = "This is critical for the execution of this program!!" fullword ascii
      $s2 = "__frame_dummy_init_array_entry" fullword ascii
      $s3 = ".note.gnu.property" fullword ascii
      $s4 = ".eh_frame_hdr" fullword ascii
      $s5 = "__FRAME_END__" fullword ascii
      $s6 = "__GNU_EH_FRAME_HDR" fullword ascii
      $s7 = "frame_dummy" fullword ascii
      $s8 = ".note.gnu.build-id" fullword ascii
      $s9 = "completed.8060" fullword ascii
      $s10 = "_IO_stdin_used" fullword ascii
      $s11 = ".note.ABI-tag" fullword ascii
      $s12 = "naughty string" fullword ascii
      $s13 = "dastardly string" fullword ascii
      $s14 = "__do_global_dtors_aux_fini_array_entry" fullword ascii
      $s15 = "__libc_start_main@@GLIBC_2.2.5" fullword ascii
      $s16 = "GLIBC_2.2.5" fullword ascii
      $s17 = "its_a_holly_jolly_variable" fullword ascii
      $s18 = "__cxa_finalize" fullword ascii
      $s19 = "HolidayHackChallenge{NotReallyAFlag}" fullword ascii
      $s20 = "__libc_csu_init" fullword ascii
   condition:
      uint32(1) == 0x02464c45 and filesize < 50KB and
      10 of them

```

```
snowball2@2ab441da8569:~$ for i in {1..100000}; do echo "A" >> the_critical_elf_app2; done
```

```
snowball2@2ab441da8569:~$ ./the_critical_elf_app2
/usr/local/bin/pre_execution.sh: line 16: ./the_critical_elf_app2: Permission denied
Machine Running..
Toy Levels: Very Merry, Terry
Naughty/Nice Blockchain Assessment: Untampered
Candy Sweetness Gauge: Exceedingly Sugarlicious
Elf Jolliness Quotient: 4a6f6c6c7920456e6f7567682c204f76657274696d6520417070726f766564
```

## IPv6

[https://docker2021.kringlecon.com/?challenge=ipv6](https://docker2021.kringlecon.com/?challenge=ipv6)

```
ping6 ff02::1 -c2
```

```
nmap 192.168.160.0/20

PORT	STATE	SERVICE
80/tcp	open	http
```

```
curl http://192.168.160.2
```

```
curl http://[2604:6000:1528:cd:d55a:f8a7:d30a:e405] --interface eth0

Connect to the other open TCP port to get the striper's activitation phrase!
```

```
nmap -6 2604:6000:1528:cd:d55a:f8a7:d30a:e405
PORT	STATE	SERVICE
80/tcp	open	http
9000/tcp	open	cslistener
```

```
curl http://[2604:6000:1528:cd:d55a:f8a7:d30a:e405]:9000 --interface eth0

PieceOnEarth
```

![[Pasted image 20211209210557.png]]

## Hoho No

[https://docker2021.kringlecon.com/?challenge=hohono](https://docker2021.kringlecon.com/?challenge=hohono)

```
2021-12-10 03:44:35 Login from 13.248.52.108 successful
2021-12-10 03:44:36 Failed login from 24.171.85.152 for alabaster
2021-12-10 03:44:36 Valid heartbeat from 35.202.33.61
2021-12-10 03:44:37 177.160.115.50: Request completed successfully
2021-12-10 03:44:37 Login from 156.174.161.46 successful
2021-12-10 03:44:38 123.178.28.242: Request completed successfully
2021-12-10 03:44:38 Login from 61.210.104.162 successful
2021-12-10 03:44:39 13.248.52.108: Request completed successfully
2021-12-10 03:44:39 223.204.171.29: Request completed successfully
2021-12-10 03:44:39 Login from 205.150.167.12 successful
2021-12-10 03:44:39 Valid heartbeat from 198.32.239.50
2021-12-10 03:44:39 Valid heartbeat from 61.150.253.141
2021-12-10 03:44:39 Valid heartbeat from 88.142.139.77
2021-12-10 03:44:40 61.210.104.162: Request completed successfully
2021-12-10 03:44:40 Login from 178.149.216.243 successful
2021-12-10 03:44:40 Valid heartbeat from 93.244.187.168
2021-12-10 03:44:41 177.160.115.50: Request completed successfully
2021-12-10 03:44:41 29.250.110.9: Request completed successfully
2021-12-10 03:44:41 46.25.157.186: Request completed successfully
2021-12-10 03:44:41 Valid heartbeat from 187.52.5.226
2021-12-10 03:44:42 Login from 18.97.52.1 successful
2021-12-10 03:44:42 Login from 187.52.5.226 successful
2021-12-10 03:44:43 Valid heartbeat from 121.18.72.243
2021-12-10 03:44:44 Valid heartbeat from 171.142.108.251
2021-12-10 03:44:45 209.40.56.203: Request completed successfully
2021-12-10 03:44:46 178.149.216.243: Request completed successfully
2021-12-10 03:44:47 172.37.107.215: Request completed successfully
2021-12-10 03:44:47 Valid heartbeat from 193.249.18.8
2021-12-10 03:44:47 Valid heartbeat from 66.80.135.159
2021-12-10 03:44:48 27.112.52.222: Request completed successfully
2021-12-10 03:44:48 Valid heartbeat from 145.194.222.206
2021-12-10 03:44:48 Valid heartbeat from 17.72.118.176
2021-12-10 03:44:49 123.24.230.14: Request completed successfully
2021-12-10 03:44:50 85.158.65.43: Request completed successfully
2021-12-10 03:44:50 Login from 119.38.182.233 successful
2021-12-10 03:44:50 Valid heartbeat from 52.30.217.127
2021-12-10 03:44:51 Valid heartbeat from 172.37.107.215
2021-12-10 03:44:52 153.70.226.177: Request completed successfully
2021-12-10 03:44:52 Failed login from 20.36.29.238 for jewel
2021-12-10 03:44:52 Login from 172.76.18.106 successful
2021-12-10 03:44:52 Valid heartbeat from 178.149.216.243
2021-12-10 03:44:53 61.210.104.162: Request completed successfully
2021-12-10 03:44:53 Login from 8.79.57.28 successful
2021-12-10 03:44:54 Valid heartbeat from 146.115.145.81
2021-12-10 03:44:54 Valid heartbeat from 19.19.38.117
2021-12-10 03:44:54 Valid heartbeat from 223.204.171.29
2021-12-10 03:44:54 Valid heartbeat from 30.31.178.66
2021-12-10 03:44:55 Valid heartbeat from 18.97.52.1
2021-12-10 03:44:55 Valid heartbeat from 182.4.251.75
2021-12-10 03:44:55 Valid heartbeat from 19.2.62.23
2021-12-10 03:44:56 Valid heartbeat from 59.92.137.254
2021-12-10 03:44:57 Login from 163.109.208.128 successful
2021-12-10 03:44:57 Valid heartbeat from 136.128.210.199
2021-12-10 03:44:57 Valid heartbeat from 178.137.105.162
2021-12-10 03:44:57 Valid heartbeat from 88.157.45.33
2021-12-10 03:44:58 61.210.104.162: Request completed successfully
2021-12-10 03:44:58 88.157.45.33: Request completed successfully
2021-12-10 03:44:58 Valid heartbeat from 193.249.18.8
2021-12-10 03:44:59 166.123.167.8: Request completed successfully
2021-12-10 03:45:00 174.180.43.247: Request completed successfully
2021-12-10 03:45:00 Login from 123.178.28.242 successful
2021-12-10 03:45:00 Login from 27.112.52.222 successful
2021-12-10 03:45:01 186.169.15.237: Request completed successfully
2021-12-10 03:45:01 221.74.89.17: Request completed successfully
2021-12-10 03:45:01 Login from 178.149.216.243 successful
2021-12-10 03:45:02 35.202.33.61: Request completed successfully
2021-12-10 03:45:02 67.67.60.237: Request completed successfully
2021-12-10 03:45:03 Login from 181.179.160.161 successful
2021-12-10 03:45:03 Valid heartbeat from 35.202.33.61
2021-12-10 03:45:04 Login from 35.202.33.61 successful
2021-12-10 03:45:04 Login from 46.25.157.186 successful
2021-12-10 03:45:05 53.203.201.90: Request completed successfully
2021-12-10 03:45:06 39.50.225.231: Request completed successfully
2021-12-10 03:45:07 78.214.208.129: Request completed successfully
2021-12-10 03:45:07 Valid heartbeat from 179.24.254.70
2021-12-10 03:45:07 Valid heartbeat from 29.250.110.9
2021-12-10 03:45:08 Login from 45.106.84.77 successful
2021-12-10 03:45:09 Failed login from 20.36.29.238 for noel
2021-12-10 03:45:09 Login from 91.19.119.97 successful
2021-12-10 03:45:09 Valid heartbeat from 38.5.243.22
2021-12-10 03:45:09 Valid heartbeat from 76.224.67.233
2021-12-10 03:45:10 80.239.20.68: Request completed successfully
2021-12-10 03:45:10 Valid heartbeat from 38.5.243.22
2021-12-10 03:45:12 Login from 61.150.253.141 successful
2021-12-10 03:45:12 Login from 62.247.227.174 successful
2021-12-10 03:45:12 Valid heartbeat from 35.202.33.61
2021-12-10 03:45:13 29.250.110.9: Request completed successfully
2021-12-10 03:45:13 Valid heartbeat from 66.80.135.159
2021-12-10 03:45:14 223.204.171.29: Request completed successfully
2021-12-10 03:45:14 4.37.133.119 sent a malformed request
2021-12-10 03:45:14 Login from 99.199.175.206 successful
2021-12-10 03:45:14 Valid heartbeat from 142.147.201.196
2021-12-10 03:45:14 Valid heartbeat from 91.19.119.97
2021-12-10 03:45:15 117.15.58.5: Request completed successfully
2021-12-10 03:45:15 178.137.105.162: Request completed successfully
2021-12-10 03:45:15 201.24.69.92: Request completed successfully
2021-12-10 03:45:15 Login from 16.169.237.184 successful
2021-12-10 03:45:17 181.179.160.161: Request completed successfully
2021-12-10 03:45:17 Login from 151.35.74.39 rejected due to unknown user name
2021-12-10 03:45:17 Valid heartbeat from 153.70.226.177
2021-12-10 03:45:18 Valid heartbeat from 63.62.152.241
2021-12-10 03:45:19 198.32.239.50: Request completed successfully
2021-12-10 03:45:19 Valid heartbeat from 142.147.201.196
2021-12-10 03:45:19 Valid heartbeat from 91.19.119.97
2021-12-10 03:45:20 13.248.52.108: Request completed successfully
2021-12-10 03:45:20 32.124.121.15: Request completed successfully
2021-12-10 03:45:20 Login from 207.192.121.95 rejected due to unknown user name
2021-12-10 03:45:21 59.92.137.254: Request completed successfully
2021-12-10 03:45:21 Login from 24.171.85.152 rejected due to unknown user name
2021-12-10 03:45:22 Valid heartbeat from 16.169.237.184
2021-12-10 03:45:22 Valid heartbeat from 88.142.139.77
2021-12-10 03:45:23 61.150.253.141: Request completed successfully
2021-12-10 03:45:24 105.204.242.11: Request completed successfully
2021-12-10 03:45:24 Login from 156.174.161.46 successful
2021-12-10 03:45:24 Login from 55.24.116.67 successful
2021-12-10 03:45:25 182.204.64.231: Request completed successfully
2021-12-10 03:45:25 Failed login from 132.91.203.231 for cupid
2021-12-10 03:45:26 Valid heartbeat from 185.238.179.144
2021-12-10 03:45:27 119.38.182.233: Request completed successfully
2021-12-10 03:45:27 19.19.38.117: Request completed successfully
2021-12-10 03:45:28 45.106.84.77: Request completed successfully
2021-12-10 03:45:29 186.169.15.237: Request completed successfully
2021-12-10 03:45:29 19.19.38.117: Request completed successfully
2021-12-10 03:45:29 Login from 177.160.115.50 successful
2021-12-10 03:45:29 Valid heartbeat from 18.97.52.1
2021-12-10 03:45:30 Login from 17.72.118.176 successful
2021-12-10 03:45:31 Login from 150.168.142.203 successful
2021-12-10 03:45:32 Login from 32.12.110.206 successful
2021-12-10 03:45:32 Valid heartbeat from 59.92.137.254
2021-12-10 03:45:33 Login from 75.146.14.186 successful
2021-12-10 03:45:34 Failed login from 207.192.121.95 for chimney
2021-12-10 03:45:34 Valid heartbeat from 33.145.101.117
2021-12-10 03:45:35 Invalid heartbeat 'alpha' from 20.36.29.238
2021-12-10 03:45:35 Login from 4.37.133.119 rejected due to unknown user name
2021-12-10 03:45:36 123.24.230.14: Request completed successfully
2021-12-10 03:45:37 67.67.60.237: Request completed successfully
2021-12-10 03:45:37 Login from 160.154.153.21 successful
2021-12-10 03:45:37 Valid heartbeat from 36.24.62.180
2021-12-10 03:45:38 Login from 5.214.82.12 successful
2021-12-10 03:45:39 204.74.39.25: Request completed successfully
2021-12-10 03:45:39 205.150.167.12: Request completed successfully
2021-12-10 03:45:39 24.171.85.152 sent a malformed request
2021-12-10 03:45:39 Valid heartbeat from 135.80.134.222
2021-12-10 03:45:39 Valid heartbeat from 91.19.119.97
2021-12-10 03:45:40 99.199.175.206: Request completed successfully
2021-12-10 03:45:40 Login from 57.11.25.251 successful
2021-12-10 03:45:41 16.169.237.184: Request completed successfully
2021-12-10 03:45:41 24.171.85.152 sent a malformed request
2021-12-10 03:45:41 Login from 36.24.62.180 successful
2021-12-10 03:45:41 Valid heartbeat from 52.30.217.127
2021-12-10 03:45:41 Valid heartbeat from 62.165.13.103
2021-12-10 03:45:41 Valid heartbeat from 67.67.60.237
2021-12-10 03:45:42 149.58.30.31: Request completed successfully
2021-12-10 03:45:42 Login from 155.100.146.81 successful
2021-12-10 03:45:44 172.37.107.215: Request completed successfully
2021-12-10 03:45:45 18.97.52.1: Request completed successfully
2021-12-10 03:45:45 Valid heartbeat from 62.247.227.174
2021-12-10 03:45:46 24.171.222.146: Request completed successfully
2021-12-10 03:45:46 Valid heartbeat from 163.109.208.128
2021-12-10 03:45:46 Valid heartbeat from 5.214.82.12
2021-12-10 03:45:47 86.99.246.187: Request completed successfully
2021-12-10 03:45:47 Login from 61.210.104.162 successful
2021-12-10 03:45:47 Valid heartbeat from 75.146.14.186
2021-12-10 03:45:48 133.48.187.32: Request completed successfully
2021-12-10 03:45:48 Login from 156.174.161.46 successful
2021-12-10 03:45:48 Valid heartbeat from 149.58.30.31
2021-12-10 03:45:49 186.190.141.98: Request completed successfully
2021-12-10 03:45:49 62.247.227.174: Request completed successfully
2021-12-10 03:45:51 119.38.182.233: Request completed successfully
2021-12-10 03:45:51 142.147.201.196: Request completed successfully
2021-12-10 03:45:51 Login from 58.21.68.25 successful
2021-12-10 03:45:51 Valid heartbeat from 160.154.153.21
2021-12-10 03:45:51 Valid heartbeat from 22.10.91.199
2021-12-10 03:45:52 Login from 166.123.167.8 successful
2021-12-10 03:45:53 218.5.24.109: Request completed successfully
2021-12-10 03:45:53 Valid heartbeat from 133.48.187.32
2021-12-10 03:45:53 Valid heartbeat from 160.154.153.21
2021-12-10 03:45:53 Valid heartbeat from 6.228.109.101
2021-12-10 03:45:54 2.15.105.193: Request completed successfully
2021-12-10 03:45:54 Valid heartbeat from 146.115.145.81
2021-12-10 03:45:55 121.68.84.183: Request completed successfully
2021-12-10 03:45:55 Login from 187.52.5.226 successful
2021-12-10 03:45:56 221.210.18.142: Request completed successfully
2021-12-10 03:45:56 Login from 88.142.139.77 successful
2021-12-10 03:45:56 Valid heartbeat from 118.111.173.136
2021-12-10 03:45:57 78.214.208.129: Request completed successfully
2021-12-10 03:45:58 Login from 182.68.252.38 successful
2021-12-10 03:45:58 Login from 6.228.109.101 successful
2021-12-10 03:45:58 Login from 69.207.232.26 successful
2021-12-10 03:45:59 Invalid heartbeat 'charlie' from 20.36.29.238
2021-12-10 03:45:59 Valid heartbeat from 146.115.145.81
2021-12-10 03:46:00 Valid heartbeat from 77.135.149.167
2021-12-10 03:46:01 Login from 123.252.205.149 successful
2021-12-10 03:46:01 Valid heartbeat from 24.171.222.146
2021-12-10 03:46:02 Login from 123.252.205.149 successful
2021-12-10 03:46:03 Failed login from 24.171.85.152 for angel
2021-12-10 03:46:03 Valid heartbeat from 223.161.7.38
2021-12-10 03:46:05 88.142.139.77: Request completed successfully
2021-12-10 03:46:05 Valid heartbeat from 50.10.113.213
2021-12-10 03:46:06 2.15.105.193: Request completed successfully
2021-12-10 03:46:06 Login from 46.25.157.186 successful
2021-12-10 03:46:06 Login from 62.247.227.174 successful
2021-12-10 03:46:08 123.178.28.242: Request completed successfully
2021-12-10 03:46:08 Login from 93.244.187.168 successful
2021-12-10 03:46:09 66.80.135.159: Request completed successfully
2021-12-10 03:46:09 Login from 163.109.208.128 successful
2021-12-10 03:46:09 Login from 6.228.109.101 successful
2021-12-10 03:46:09 Valid heartbeat from 9.186.28.19
2021-12-10 03:46:10 105.204.242.11: Request completed successfully
2021-12-10 03:46:10 18.97.52.1: Request completed successfully
2021-12-10 03:46:10 Login from 93.244.187.168 successful
2021-12-10 03:46:11 198.32.239.50: Request completed successfully
2021-12-10 03:46:11 Login from 32.124.121.15 successful
2021-12-10 03:46:11 Valid heartbeat from 183.40.116.91
2021-12-10 03:46:12 6.228.109.101: Request completed successfully
2021-12-10 03:46:12 69.207.232.26: Request completed successfully
2021-12-10 03:46:12 88.157.45.33: Request completed successfully
2021-12-10 03:46:12 Valid heartbeat from 32.253.28.245
2021-12-10 03:46:13 Login from 24.171.222.146 successful
2021-12-10 03:46:13 Valid heartbeat from 166.123.167.8
2021-12-10 03:46:16 61.150.253.141: Request completed successfully
2021-12-10 03:46:16 Login from 178.149.216.243 successful
2021-12-10 03:46:16 Login from 181.56.187.22 successful
2021-12-10 03:46:17 166.123.167.8: Request completed successfully
2021-12-10 03:46:17 185.238.179.144: Request completed successfully
2021-12-10 03:46:17 207.192.121.95 sent a malformed request
2021-12-10 03:46:17 Login from 177.160.115.50 successful
2021-12-10 03:46:17 Login from 186.169.15.237 successful
2021-12-10 03:46:18 Login from 78.214.208.129 successful
2021-12-10 03:46:19 Invalid heartbeat 'delta' from 24.171.85.152
2021-12-10 03:46:19 Login from 217.127.18.199 successful
2021-12-10 03:46:19 Valid heartbeat from 182.4.251.75
2021-12-10 03:46:20 Login from 186.169.15.237 successful
2021-12-10 03:46:21 2.15.105.193: Request completed successfully
2021-12-10 03:46:22 Valid heartbeat from 134.56.118.5
2021-12-10 03:46:22 Valid heartbeat from 181.87.134.84
2021-12-10 03:46:23 62.247.227.174: Request completed successfully
2021-12-10 03:46:23 Valid heartbeat from 169.100.8.195
2021-12-10 03:46:23 Valid heartbeat from 75.146.14.186
2021-12-10 03:46:24 Login from 75.7.156.40 rejected due to unknown user name
2021-12-10 03:46:24 Valid heartbeat from 59.92.137.254
2021-12-10 03:46:25 Login from 121.2.64.144 successful
2021-12-10 03:46:25 Login from 142.147.201.196 successful
2021-12-10 03:46:26 Valid heartbeat from 102.145.150.172
2021-12-10 03:46:26 Valid heartbeat from 127.17.112.246
2021-12-10 03:46:26 Valid heartbeat from 2.15.105.193
2021-12-10 03:46:27 Login from 102.145.150.172 successful
2021-12-10 03:46:27 Login from 66.7.116.215 successful
2021-12-10 03:46:27 Valid heartbeat from 193.249.18.8
2021-12-10 03:46:28 123.178.28.242: Request completed successfully
2021-12-10 03:46:28 Login from 45.68.108.221 successful
2021-12-10 03:46:28 Login from 48.163.144.209 successful
2021-12-10 03:46:28 Valid heartbeat from 52.30.217.127
2021-12-10 03:46:29 18.97.52.1: Request completed successfully
2021-12-10 03:46:29 29.250.110.9: Request completed successfully
2021-12-10 03:46:29 Login from 177.160.115.50 successful
2021-12-10 03:46:29 Login from 50.10.113.213 successful
2021-12-10 03:46:30 181.87.134.84: Request completed successfully
2021-12-10 03:46:30 38.5.243.22: Request completed successfully
2021-12-10 03:46:30 Invalid heartbeat 'bravo' from 24.171.85.152
2021-12-10 03:46:30 Login from 204.100.99.117 successful
2021-12-10 03:46:30 Valid heartbeat from 186.169.15.237
2021-12-10 03:46:31 36.24.62.180: Request completed successfully
2021-12-10 03:46:31 Valid heartbeat from 171.142.108.251
2021-12-10 03:46:32 Failed login from 20.36.29.238 for tinsel
2021-12-10 03:46:32 Login from 91.19.119.97 successful
2021-12-10 03:46:33 185.123.112.105: Request completed successfully
2021-12-10 03:46:33 Login from 106.25.143.104 successful
2021-12-10 03:46:33 Login from 67.67.60.237 successful
2021-12-10 03:46:35 151.35.74.39 sent a malformed request
2021-12-10 03:46:35 75.7.156.40 sent a malformed request
2021-12-10 03:46:35 Login from 179.24.254.70 successful
2021-12-10 03:46:37 62.165.13.103: Request completed successfully
2021-12-10 03:46:37 Login from 123.24.230.14 successful
2021-12-10 03:46:39 Login from 119.38.182.233 successful
2021-12-10 03:46:39 Valid heartbeat from 6.228.109.101
2021-12-10 03:46:40 Login from 132.91.203.231 rejected due to unknown user name
2021-12-10 03:46:40 Valid heartbeat from 63.62.152.241
2021-12-10 03:46:42 86.99.246.187: Request completed successfully
2021-12-10 03:46:43 Login from 78.214.208.129 successful
2021-12-10 03:46:43 Valid heartbeat from 19.19.38.117
2021-12-10 03:46:44 35.202.33.61: Request completed successfully
2021-12-10 03:46:44 36.24.62.180: Request completed successfully
2021-12-10 03:46:44 Login from 105.117.40.61 successful
2021-12-10 03:46:44 Valid heartbeat from 185.123.112.105
2021-12-10 03:46:45 Valid heartbeat from 75.146.14.186
2021-12-10 03:46:46 Valid heartbeat from 62.165.13.103
2021-12-10 03:46:47 221.210.18.142: Request completed successfully
2021-12-10 03:46:49 16.169.237.184: Request completed successfully
2021-12-10 03:46:49 166.123.167.8: Request completed successfully
2021-12-10 03:46:49 88.157.45.33: Request completed successfully
2021-12-10 03:46:50 119.38.182.233: Request completed successfully
2021-12-10 03:46:50 Login from 207.192.121.95 rejected due to unknown user name
2021-12-10 03:46:50 Valid heartbeat from 53.203.201.90
2021-12-10 03:46:51 18.97.52.1: Request completed successfully
2021-12-10 03:46:51 Login from 218.5.24.109 successful
2021-12-10 03:46:51 Valid heartbeat from 160.84.251.38
2021-12-10 03:46:51 Valid heartbeat from 67.67.60.237
2021-12-10 03:46:52 198.32.239.50: Request completed successfully
2021-12-10 03:46:52 92.111.35.80: Request completed successfully
2021-12-10 03:46:52 Valid heartbeat from 92.204.212.209
2021-12-10 03:46:53 169.100.8.195: Request completed successfully
2021-12-10 03:46:53 187.52.5.226: Request completed successfully
2021-12-10 03:46:53 63.62.152.241: Request completed successfully
2021-12-10 03:46:53 Login from 178.149.216.243 successful
2021-12-10 03:46:54 Valid heartbeat from 198.32.239.50
2021-12-10 03:46:55 32.124.121.15: Request completed successfully
2021-12-10 03:46:55 Login from 178.149.216.243 successful
2021-12-10 03:46:55 Valid heartbeat from 61.150.253.141
2021-12-10 03:46:55 Valid heartbeat from 72.136.154.109
2021-12-10 03:46:55 Valid heartbeat from 9.186.28.19
2021-12-10 03:46:56 2.15.105.193: Request completed successfully
2021-12-10 03:46:56 78.214.208.129: Request completed successfully
2021-12-10 03:46:56 78.214.208.129: Request completed successfully
2021-12-10 03:46:58 Failed login from 4.37.133.119 for piney
2021-12-10 03:46:58 Valid heartbeat from 27.112.52.222
2021-12-10 03:46:59 117.15.58.5: Request completed successfully
2021-12-10 03:46:59 193.78.6.45: Request completed successfully
2021-12-10 03:46:59 Login from 18.102.148.117 successful
2021-12-10 03:46:59 Login from 193.249.18.8 successful
2021-12-10 03:47:00 105.117.40.61: Request completed successfully
2021-12-10 03:47:00 90.168.83.22: Request completed successfully
2021-12-10 03:47:00 Invalid heartbeat 'alpha' from 20.36.29.238
2021-12-10 03:47:01 18.102.148.117: Request completed successfully
2021-12-10 03:47:01 91.19.119.97: Request completed successfully
2021-12-10 03:47:01 91.19.119.97: Request completed successfully
2021-12-10 03:47:01 Invalid heartbeat 'bravo' from 24.171.85.152
2021-12-10 03:47:01 Login from 154.176.75.5 successful
2021-12-10 03:47:01 Login from 45.68.108.221 successful
2021-12-10 03:47:02 17.72.118.176: Request completed successfully
2021-12-10 03:47:02 18.97.52.1: Request completed successfully
2021-12-10 03:47:04 Valid heartbeat from 18.102.148.117
2021-12-10 03:47:06 33.145.101.117: Request completed successfully
2021-12-10 03:47:06 Login from 27.112.52.222 successful
2021-12-10 03:47:07 Failed login from 132.91.203.231 for krampus
2021-12-10 03:47:07 Valid heartbeat from 150.168.142.203
2021-12-10 03:47:08 Login from 185.123.112.105 successful
2021-12-10 03:47:08 Login from 193.78.6.45 successful
2021-12-10 03:47:08 Login from 53.203.201.90 successful
2021-12-10 03:47:08 Valid heartbeat from 153.70.226.177
2021-12-10 03:47:09 91.19.119.97: Request completed successfully
2021-12-10 03:47:10 Login from 160.84.251.38 successful
2021-12-10 03:47:11 Login from 172.76.18.106 successful
2021-12-10 03:47:13 91.19.119.97: Request completed successfully
2021-12-10 03:47:13 Login from 171.142.108.251 successful
2021-12-10 03:47:14 106.25.143.104: Request completed successfully
2021-12-10 03:47:14 Login from 172.37.107.215 successful
2021-12-10 03:47:15 149.58.30.31: Request completed successfully
2021-12-10 03:47:15 Valid heartbeat from 17.72.118.176
2021-12-10 03:47:16 123.252.205.149: Request completed successfully
2021-12-10 03:47:16 Login from 38.5.243.22 successful
2021-12-10 03:47:17 39.50.225.231: Request completed successfully
2021-12-10 03:47:17 Valid heartbeat from 172.76.18.106
2021-12-10 03:47:17 Valid heartbeat from 5.214.82.12
2021-12-10 03:47:18 102.145.150.172: Request completed successfully
2021-12-10 03:47:18 174.180.43.247: Request completed successfully
2021-12-10 03:47:19 173.155.189.192: Request completed successfully
2021-12-10 03:47:19 Valid heartbeat from 177.160.115.50
2021-12-10 03:47:21 105.117.40.61: Request completed successfully
2021-12-10 03:47:21 166.123.167.8: Request completed successfully
2021-12-10 03:47:21 Login from 185.123.112.105 successful
2021-12-10 03:47:21 Login from 62.165.13.103 successful
2021-12-10 03:47:22 Valid heartbeat from 178.137.105.162
2021-12-10 03:47:24 182.204.64.231: Request completed successfully
2021-12-10 03:47:24 32.253.28.245: Request completed successfully
2021-12-10 03:47:24 Valid heartbeat from 186.169.15.237
2021-12-10 03:47:25 Failed login from 132.91.203.231 for eve
2021-12-10 03:47:26 117.15.58.5: Request completed successfully
2021-12-10 03:47:26 17.72.118.176: Request completed successfully
2021-12-10 03:47:26 2.15.105.193: Request completed successfully
2021-12-10 03:47:26 92.204.212.209: Request completed successfully
2021-12-10 03:47:26 Login from 24.171.222.146 successful
2021-12-10 03:47:26 Login from 62.165.13.103 successful
2021-12-10 03:47:27 27.112.52.222: Request completed successfully
2021-12-10 03:47:27 92.111.35.80: Request completed successfully
2021-12-10 03:47:28 134.56.118.5: Request completed successfully
2021-12-10 03:47:28 Invalid heartbeat 'alpha' from 75.7.156.40
2021-12-10 03:47:28 Login from 121.2.64.144 successful
2021-12-10 03:47:28 Login from 174.180.43.247 successful
2021-12-10 03:47:28 Login from 93.244.187.168 successful
2021-12-10 03:47:28 Valid heartbeat from 160.84.251.38
2021-12-10 03:47:29 24.171.222.146: Request completed successfully
2021-12-10 03:47:29 Failed login from 75.7.156.40 for bubble
2021-12-10 03:47:29 Login from 66.80.135.159 successful
2021-12-10 03:47:29 Valid heartbeat from 76.224.67.233
2021-12-10 03:47:30 134.56.118.5: Request completed successfully
2021-12-10 03:47:33 171.142.108.251: Request completed successfully
2021-12-10 03:47:33 Valid heartbeat from 29.250.110.9
2021-12-10 03:47:34 Valid heartbeat from 19.2.62.23
2021-12-10 03:47:34 Valid heartbeat from 88.142.139.77
2021-12-10 03:47:35 Login from 156.174.161.46 successful
2021-12-10 03:47:36 91.19.119.97: Request completed successfully
2021-12-10 03:47:36 Login from 4.37.133.119 rejected due to unknown user name
2021-12-10 03:47:36 Valid heartbeat from 62.165.13.103
2021-12-10 03:47:38 36.24.62.180: Request completed successfully
2021-12-10 03:47:38 Valid heartbeat from 181.179.160.161
2021-12-10 03:47:40 22.10.91.199: Request completed successfully
2021-12-10 03:47:40 Invalid heartbeat 'alpha' from 132.91.203.231
2021-12-10 03:47:40 Login from 174.180.43.247 successful
2021-12-10 03:47:40 Valid heartbeat from 62.165.13.103
2021-12-10 03:47:41 Login from 66.80.135.159 successful
2021-12-10 03:47:42 Login from 187.52.5.226 successful
2021-12-10 03:47:42 Login from 61.210.104.162 successful
2021-12-10 03:47:43 118.111.173.136: Request completed successfully
2021-12-10 03:47:43 Failed login from 24.171.85.152 for noel
2021-12-10 03:47:43 Login from 16.169.237.184 successful
2021-12-10 03:47:44 Login from 67.67.60.237 successful
2021-12-10 03:47:45 Login from 142.147.201.196 successful
2021-12-10 03:47:45 Login from 207.192.121.95 rejected due to unknown user name
2021-12-10 03:47:45 Valid heartbeat from 45.68.108.221
2021-12-10 03:47:47 105.204.242.11: Request completed successfully
2021-12-10 03:47:47 38.5.243.22: Request completed successfully
2021-12-10 03:47:47 Login from 4.37.133.119 rejected due to unknown user name
2021-12-10 03:47:48 Login from 163.109.208.128 successful
```

```
[Definition]
failregex = [Ff]ailed login from <HOST> for .+
[Ii]nvalid heartbeat '.+' from <HOST>
<HOST> sent a malformed request
Login from <HOST> rejected due to unknown user name
```

```
root@96147502b72b:~# cat /etc/fail2ban/action.d/hohono.conf
[Definition]
actionban = /etc/naughtylist add <ip>
```

```
[hohono]
enabled = true
logpath = /var/log/hohono.log
findtime = 60m
maxretry = 10
bantime = 30m
filter = hohono
action = hohono
```

## Hero

[https://docker2021.kringlecon.com/?challenge=hero](https://docker2021.kringlecon.com/?challenge=hero)

![](./images/hero1.png)
![](./images/hero2.png)
![](./images/hero3.png)

```
single_player_mode = true
```

## Exif

[https://docker2021.kringlecon.com/?challenge=exif](https://docker2021.kringlecon.com/?challenge=exif)

```
exiftool * | grep Jack -B50

2021-12-21.dox
```

## Elf Code Python

[https://elfcode21.kringlecastle.com?challenge=elfcode](https://elfcode21.kringlecastle.com?challenge=elfcode)

### 1

```python
import elf, munchkins, levers, lollipops, yeeters, pits
elf.moveLeft(10)
elf.moveUp(10)
```

### 2

```python
import elf, munchkins, levers, lollipops, yeeters, pits
elf.moveTo(lollipops.get(1).position)
elf.moveTo(lollipops.get(0).position)
elf.moveLeft(3)
elf.moveUp(6)
```

### 3

```
import elf, munchkins, levers, lollipops, yeeters, pits
lever0 = levers.get(0)
lollipop0 = lollipops.get(0)
elf.moveTo(lever0.position)
sum = lever0.data() + 2
lever0.pull(sum)
elf.moveTo(lollipop0.position)
elf.moveUp(10)
```

### 4

```python
import elf, munchkins, levers, lollipops, yeeters, pits
# Complete the code below:
lever0, lever1, lever2, lever3, lever4 = levers.get()
# Move onto lever4
elf.moveLeft(2)
# This lever wants a str object:
lever4.pull("A String")
# Need more code below:
elf.moveTo(lever3.position)
lever3.pull(True)
elf.moveTo(lever2.position)
lever2.pull(0)
elf.moveTo(lever1.position)
lever1.pull(['1','2'])
elf.moveTo(lever0.position)
lever0.pull({'1':'1','2':'1'})
elf.moveUp(2)
```

### 5

```
import elf, munchkins, levers, lollipops, yeeters, pits
# Fix/Complete Code below
lever0, lever1, lever2, lever3, lever4 = levers.get()
elf.moveTo(lever4.position)
lever4.pull(str(lever4.data()) + " concatenate")
elf.moveTo(lever3.position)
lever3.pull(not lever3.data())
elf.moveTo(lever2.position)
lever2.pull(lever2.data() + 1)
elf.moveTo(lever1.position)
lever1Ans = lever1.data()
lever1Ans.append(1)
lever1.pull(lever1Ans)
elf.moveTo(lever0.position)
lever0Ans = lever0.data()
lever0Ans.update({"strkey":"strvalue"})
lever0.pull(lever0Ans)
elf.moveUp(2)
```

### 6

```python
import elf, munchkins, levers, lollipops, yeeters, pits
# Fix/Complete the below code
lever = levers.get(0)
data = lever.data()
print(type(data))
if type(data) == bool:
    data = not bool
elif type(data) == int:
    data = data + 1
elif type(data) == list:
    for i in data:
        data[i] = data[i] + 1
elif type(data) == str:
    data = data+data
elif type(data) == dict:
    data['a'] = data['a'] + 1

elf.moveTo(lever.position)
lever.pull(data)
elf.moveUp(2)
```

### 7

```python
import elf, munchkins, levers, lollipops, yeeters, pits
for num in range(2):
    elf.moveLeft(1)
    elf.moveUp(11)
    elf.moveLeft(2)
    elf.moveDown(11)
    elf.moveLeft(1)

elf.moveLeft(2)
elf.moveUp(10)
```

### 8

```python
import elf, munchkins, levers, lollipops, yeeters, pits
q = munchkins.get(0).ask()
for i in q:
    if q[i] == 'lollipop':
        ans = i
all_lollipops = lollipops.get()
for lollipop in all_lollipops:
    elf.moveTo(lollipop.position)
elf.moveLeft(8)
elf.moveUp(2)
munchkins.get(0).answer(ans)
elf.moveUp(2)
```

## Logic Munchers

[https://logic.kringlecastle.com/?challenge=logicmunchers](https://logic.kringlecastle.com/?challenge=logicmunchers)

Quick win by posting this in the Console.

```
chompyFlexTime = 0
```

## Grepping for Gold

[https://docker2021.kringlecon.com/?challenge=gnmap](https://docker2021.kringlecon.com/?challenge=gnmap)

Answer all the questions in the quizme executable:

- What port does 34.76.1.22 have open?
- grep 34.76.1.22 bigscan.gnmap
- What port does 34.77.207.226 have open?
- How many hosts appear "Up" in the scan?
- How many hosts have a web port open? (Let's just use TCP ports 80, 443, and 8080)
- How many hosts with status Up have no (detected) open TCP ports?
- What's the greatest number of TCP ports any one host has open?

```
elf@28e3fcf7f8a9:~$ grep 34.76.1.22 bigscan.gnmap
Host: 34.76.1.22 ()     Status: Up
Host: 34.76.1.22 ()     Ports: 62078/open/tcp//iphone-sync///      Ignored State: closed (999)
elf@28e3fcf7f8a9:~$ grep 34.77.207.226 bigscan.gnmap
Host: 34.77.207.226 ()     Status: Up
Host: 34.77.207.226 ()     Ports: 8080/open/tcp//http-proxy///      Ignored State: filtered (999
elf@28e3fcf7f8a9:~$ grep 'Status: Up' bigscan.gnmap | uniq | wc -l
26054
elf@28e3fcf7f8a9:~$ grep -E '\s80/|\s443/|\s8080/' bigscan.gnmap  | wc -l
14372
grep tcp bigscan.gnmap  | wc -l
25652
26504-25652

elf@28e3fcf7f8a9:~$ grep -E "(open.+){12}" bigscan.gnmap
Host: 34.76.237.4 ()     Ports: 21/open/tcp//ftp///, 22/open/tcp//ssh///, 25/open/tcp//smtp///, 80/open/tcp//http///, 110/open/tcp//pop3///, 143/open/tcp//imap///, 443/open/tcp//https///, 993/open/tcp//imaps///, 995/open/tcp//pop3s///, 5060/open/tcp//sip///, 5900/open/tcp//vnc///, 8080/open/tcp//http-proxy///      Ignored State: closed (988)
Host: 34.77.152.226 ()     Ports: 22/open/tcp//ssh///, 25/open/tcp//smtp///, 80/open/tcp//http///, 110/open/tcp//pop3///, 135/open/tcp//msrpc///, 137/open/tcp//netbios-ns///, 139/open/tcp//netbios-ssn///, 143/open/tcp//imap///, 445/open/tcp//microsoft-ds///, 993/open/tcp//imaps///, 995/open/tcp//pop3s///, 3389/open/tcp//ms-wbt-server///      Ignored State: closed (988)
Host: 34.78.10.40 ()     Ports: 21/open/tcp//ftp///, 22/open/tcp//ssh///, 25/open/tcp//smtp///, 110/open/tcp//pop3///, 135/open/tcp//msrpc///, 137/open/tcp//netbios-ns///, 139/open/tcp//netbios-ssn///, 143/open/tcp//imap///, 445/open/tcp//microsoft-ds///, 993/open/tcp//imaps///, 995/open/tcp//pop3s///, 3389/open/tcp//ms-wbt-server///      Ignored State: filtered (988)
Host: 34.79.22.38 ()     Ports: 21/open/tcp//ftp///, 25/open/tcp//smtp///, 80/open/tcp//http///, 110/open/tcp//pop3///, 135/open/tcp//msrpc///, 137/open/tcp//netbios-ns///, 139/open/tcp//netbios-ssn///, 143/open/tcp//imap///, 445/open/tcp//microsoft-ds///, 993/open/tcp//imaps///, 995/open/tcp//pop3s///, 3389/open/tcp//ms-wbt-server///      Ignored State: closed (988)
Host: 34.79.94.34 ()     Ports: 21/open/tcp//ftp///, 25/open/tcp//smtp///, 80/open/tcp//http///, 110/open/tcp//pop3///, 135/open/tcp//msrpc///, 137/open/tcp//netbios-ns///, 139/open/tcp//netbios-ssn///, 143/open/tcp//imap///, 445/open/tcp//microsoft-ds///, 993/open/tcp//imaps///, 995/open/tcp//pop3s///, 8080/open/tcp//http-proxy///      Ignored State: closed (988)
```

## IMDS Exploration

[https://docker2021.kringlecon.com/?challenge=imds](https://docker2021.kringlecon.com/?challenge=imds)

```json
{
    "top_pane_intro":"\uD83C\uDF84\uD83C\uDF84\uD83C\uDF84\n\uD83C\uDF84\uD83C\uDF84\uD83C\uDF8
4 Prof. Petabyte here. In this lesson you'll continue to build your cloud asset skills,\n\uD83C
\uDF84\uD83C\uDF84\uD83C\uDF84 interacting with the Instance Metadata Service (IMDS) using curl
.\n\uD83C\uDF84\uD83C\uDF84\uD83C\uDF84\n\uD83C\uDF84\uD83C\uDF84\uD83C\uDF84 If you get stuck,
 run 'hint' for assitance.\n\uD83C\uDF84\uD83C\uDF84\uD83C\uDF84\n",
    "bottom_pane_intro":"Are you ready to begin? [Y]es: ",
    "progressbar_char":"\uD83C\uDF6C",
    "finale":"\uD83C\uDF6C\uD83C\uDF6C\uD83C\uDF6C\uD83C\uDF6CCongratulations!\uD83C\uDF6C\uD83
C\uDF6C\uD83C\uDF6C\uD83C\uDF6C\nYou've completed the lesson on Instance Metadata interaction.
Run 'exit' to close.",
    "questions":[
        {
            "cmds_on_begin":[
                "sudo /opt/imds/imds.sh 2>&1 >/tmp/.imds.log &",
                "sudo rm /etc/sudoers"
            ],
            "question":"The Instance Metadata Service (IMDS) is a virtual server for cloud asse
ts at the IP address\n169.254.169.254. Send a couple ping packets to the server.",
            "hint":"Run 'ping 169.254.169.254' to ping the server. Press CTRL+C to stop the com
mand after a few\nseconds.",
            "type":"str",
            "str":["--- 169.254.169.254 ping statistics ---"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"IMDS provides information about currently running virtual machine insta
nces. You can use it\nto manage and configure cloud nodes. IMDS is used by all major cloud prov
iders.\nRun 'next' to continue.",
            "hint":"Run 'next' to continue with the next lesson.",
            "type":"str",
            "str":["aoOHcaMYbCkIeHiSpZKUc"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Developers can automate actions using IMDS. We'll interact with the server using the cURL\ntool. Run 'curl http://169.254.169.254' to access IMDS data.",
            "hint":"Run 'curl http://169.254.169.254' to access the IMDS server.",
            "type":"str",
            "str":["latest"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Different providers will have different formats for IMDS data. We're us
ing an AWS-compatible\nIMDS server that returns 'latest' as the default response. Access the 'l
atest' endpoint.\nRun 'curl http://169.254.169.254/latest'",
            "hint":"Run 'curl http://169.254.169.254/latest' to access the IMDS server /latest
endpoint.",
            "type":"str",
            "str":["dynamic", "meta-data"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"IMDS returns two new endpoints: dynamic and meta-data. Let's start with
 the dynamic\nendpoint, which provides information about the instance itself. Repeat the reques
t\nto access the dynamic endpoint: 'curl http://169.254.169.254/latest/dynamic'.",
            "hint":"Run 'curl http://169.254.169.254/latest/dynamic' to access the IMDS server
/latest/dynamic\nendpoint.",
            "type":"str",
            "str":["fws/instance-monitoring", "instance-identity/document", "instance-identity/
pkcs7", "instance-identity/signature"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"The instance identity document can be used by developers to understand
the instance details.\nRepeat the request, this time requesting the instance-identity/document
resource:\n'curl http://169.254.169.254/latest/dynamic/instance-identity/document'.",
            "hint":"Run 'curl http://169.254.169.254/latest/dynamic/instance-identity/document'
 to access the\nIMDS server instance identity document.",
            "type":"str",
            "str":["accountId", "imageId", "availabilityZone", "np-north-1f"],
			"cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Much of the data retrieved from IMDS will be returned in JavaScript Obj
ect Notation (JSON)\nformat. Piping the output to 'jq' will make the content easier to read.\nR
e-run the previous command, sending the output to JQ: 'curl http://169.254.169.254/latest/dynam
ic/instance-identity/document | jq'",
            "hint":"Press the up arrow to re-run the last command, added '| jq' to the end of t
he command line:\ncurl http://169.254.169.254/latest/dynamic/instance-identity/document | jq",
            "type":"rgx",
            "rgx":["^  \"accountId\": \"PCRVQVHN4S0L4V2TE\",$"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Here we see several details about the instance when it was launched. De
velopers can use this\ninformation to optimize applications based on the instance launch parame
ters.\nRun 'next' to continue.",
            "hint":"Run 'next' to continue with the next lesson.",
            "type":"str",
            "str":["aoOHcaMYbCkIeHiSpZKUc"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"In addition to dynamic parameters set at launch, IMDS offers metadata a
bout the instance as\nwell. Examine the metadata elements available:\n'curl http://169.254.169.
254/latest/meta-data'",
            "hint":"Run 'curl http://169.254.169.254/latest/meta-data' to access the IMDS metad
ata endpoint.",
            "type":"str",
            "str":["public-hostname"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"By accessing the metadata elements, a developer can interrogate information about the system. Take a look at the public-hostname element:\n'curl http://169.254.169.25
4/latest/meta-data/public-hostname'",
            "hint":"Run 'curl http://169.254.169.254/latest/meta-data/public-hostname' to acces
s the VM public\nhostname information.",
            "type":"str",
            "str":["amazonaws.comelfu"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Many of the data elements returned won't include a trailing newline, wh
ich causes the\nresponse to blend into the prompt. Re-run the prior command, adding '; echo' to
 the end of\nthe command. This will add a new line character to the response.",
            "hint":"Run 'curl http://169.254.169.254/latest/meta-data/public-hostname; echo' to
 access the VM\npublic hostname information with a newline at the end.",
            "type":"rgx",
            "rgx":["^ec2-192-0-2-54.compute-1.amazonaws.com$"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"There is a whole lot of information that can be retrieved from the IMDS
 server. Even AWS\nIdentity and Access Management (IAM) credentials! Request the endpoint\n'htt
p://169.254.169.254/latest/meta-data/iam/security-credentials' to see the instance IAM role.",
            "hint":"Run 'curl http://169.254.169.254/latest/meta-data/iam/security-credentials;
 echo' to see the\nIAM role information.",
            "type":"rgx",
            "rgx":["^elfu-deploy-role"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"Once you know the role name, you can request the AWS keys associated wi
th the role. Request\nthe endpoint 'http://169.254.169.254/latest/meta-data/iam/security-creden
tials/elfu-deploy-role' to get the instance AWS keys.",
            "hint":"Run 'curl http://169.254.169.254/latest/meta-data/iam/security-credentials;
 echo' to see the\nIAM role information.",
            "type":"str","str":["CGgQcSdERePvGgr058r3PObPq3+0CfraKcsLREpX"],
            "cmds_on_complete":false
        },

        {
            "cmds_on_begin":[],
            "question":"So far, we've been interacting with the IMDS server using IMDSv1, which
 does not require\nauthentication. Optionally, AWS users can turn on IMDSv2 that requires authe
ntication. This\nis more secure, but not on by default.\nRun 'next' to continue.",
            "hint":"Run 'next' to continue with the next lesson.",
            "type":"str",
            "str":["aoOHcaMYbCkIeHiSpZKUc"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":["echo 'TOKEN=`curl -X PUT \"http://169.254.169.254/latest/api/toke
n\" -H \"X-aws-ec2-metadata-token-ttl-seconds: 21600\"`' >/home/elfu/gettoken.sh; chmod 755 /ho
me/elfu/gettoken.sh; chown elfu:elfu /home/elfu/gettoken.sh"],
            "question":"For IMDSv2 access, you must request a token from the IMDS server using
the\nX-aws-ec2-metadata-token-ttl-seconds header to indicate how long you want the token to be\
nused for (between 1 and 21,600 secods).\nExamine the contents of the 'gettoken.sh' script in t
he current directory using 'cat'.",
            "hint":"Run 'cat gettoken.sh' to display the script contents.",
            "type":"str",
            "str":["X-aws-ec2-metadata-token-ttl-seconds: 21600"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"This script will retrieve a token from the IMDS server and save it in t
he environment\nvariable TOKEN. Import it into your environment by running 'source gettoken.sh'
.",
            "hint":"Run 'source gettoken.sh' to run the script and set the environment variable
 in your shell.",
            "type":"str",
            "str":["source", "gettoken.sh", "Total"],
            "cmds_on_complete":false
        },
		{
            "cmds_on_begin":[],
            "question":"Now, the IMDS token value is stored in the environment variable TOKEN.
Examine the contents\nof the token by running 'echo $TOKEN'.",
            "hint":"Run 'echo $TOKEN' to display the TOKEN environment variable.",
            "type":"rgx",
            "rgx":["=$"],
            "cmds_on_complete":false
        },
        {
            "cmds_on_begin":[],
            "question":"With the IMDS token, you can make an IMDSv2 request by adding the X-aws
-ec2-metadata-token\nheader to the curl request. Access the metadata region information in an\n
IMDSv2 request: 'curl -H \"X-aws-ec2-metadata-token: $TOKEN\" http://169.254.169.254/latest/met
a-data/placement/region'",
            "hint":"Be sure to capitalize the curl header X-aws-ec2-metadata-token-ttl-seconds
and the token\nvariable $TOKEN correctly with the request.",
            "type":"str",
            "str":["np-north-1"],
            "cmds_on_complete":false
        }

    ]
}
```

## kottoncandy

```
================================================================================

Please, we need your help! The cotton candy machine is broken!

We replaced the SD card in the Cranberry Pi that controls it and reinstalled the
software. Now it's complaining that it can't find a registration file!

Perhaps you could figure out what the cotton candy software is looking for...

================================================================================
```

```
kotton_kandy_co@d2de0e82f6cc:~$ ltrace ./make_the_candy
fopen("registration.json", "r")                           = 0
puts("Unable to open configuration fil"...Unable to open configuration file.
)               = 35
+++ exited (status 1) +++
```

```
kotton_kandy_co@d2de0e82f6cc:~$ touch registration.json
kotton_kandy_co@d2de0e82f6cc:~$ ltrace ./make_the_candy
fopen("registration.json", "r")                           = 0x560c5cfd8260
getline(0x7ffcc524d120, 0x7ffcc524d128, 0x560c5cfd8260, 0x7ffcc524d128) = -1
puts("Unregistered - Exiting."Unregistered - Exiting.
)                           = 24
+++ exited (status 1) +++
```

```
kotton_kandy_co@85e1285a1860:~$ ltrace -i ./make_the_candy
[0x55fbe6e74bbc] fopen("registration.json", "r")          = 0x55fbe79dd260
[0x55fbe6e74c8d] getline(0x7fff1d679e70, 0x7fff1d679e78, 0x55fbe79dd260, 0x7fff1d679e78) = 1
[0x55fbe6e74c03] strstr("\n", "Registration")             = nil
[0x55fbe6e74c8d] getline(0x7fff1d679e70, 0x7fff1d679e78, 0x55fbe79dd260, 0x7fff1d679e78) = -1
[0x55fbe6e74cc3] puts("Unregistered - Exiting."Unregistered - Exiting.
)          = 24
[0xffffffffffffffff] +++ exited (status 1) +++
```

```
kotton_kandy_co@85e1285a1860:~$ echo "Registration" > registration.json
kotton_kandy_co@85e1285a1860:~$ ltrace -i ./make_the_candy
[0x55d81cdf4bbc] fopen("registration.json", "r")          = 0x55d81e147260
[0x55d81cdf4c8d] getline(0x7ffed72de0d0, 0x7ffed72de0d8, 0x55d81e147260, 0x7ffed72de0d8) = 13
[0x55d81cdf4c03] strstr("Registration\n", "Registration") = "Registration\n"
[0x55d81cdf4c28] strchr("Registration\n", ':')            = nil
[0x55d81cdf4c8d] getline(0x7ffed72de0d0, 0x7ffed72de0d8, 0x55d81e147260, 0x7ffed72de0d8) = -1
[0x55d81cdf4cc3] puts("Unregistered - Exiting."Unregistered - Exiting.
)          = 24
[0xffffffffffffffff] +++ exited (status 1) +++
```

```
kotton_kandy_co@85e1285a1860:~$ echo "Registration:" > registration.json
kotton_kandy_co@85e1285a1860:~$ ltrace -i ./make_the_candy
[0x5649de8eabbc] fopen("registration.json", "r")          = 0x5649dec57260
[0x5649de8eac8d] getline(0x7ffd880f2210, 0x7ffd880f2218, 0x5649dec57260, 0x7ffd880f2218) = 14
[0x5649de8eac03] strstr("Registration:\n", "Registration") = "Registration:\n"
[0x5649de8eac28] strchr("Registration:\n", ':')           = ":\n"
[0x5649de8eac52] strstr(":\n", "True")                    = nil
[0x5649de8eac8d] getline(0x7ffd880f2210, 0x7ffd880f2218, 0x5649dec57260, 0x7ffd880f2218) = -1
[0x5649de8eacc3] puts("Unregistered - Exiting."Unregistered - Exiting.
)          = 24
```

```
kotton_kandy_co@85e1285a1860:~$ echo "Registration:True" > registration.json
```

```
       *                          *
      *                            *
     *                              *
     *                              *
     *                              *
     *                              *
      *                            *
       *                          *
        *                        *
         *                      *
          *                    *
           *                  *
            *                *
             *              *
              *            *
               *          *
                *        *
                 *      *
                  *    *
                   *  *
                    **
         Candy making in progress
```
